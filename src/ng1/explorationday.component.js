(function(){
    function explorationdayController(explorationdayService, $location){
        var vm = this;
        vm.title = 'Explorationday'
        console.log('test');
        console.log($location.path());
        explorationdayService.getData().then(function(response){
            vm.data = response;
        }, function(error){
           console.log(error); 
        });
        
    }

    explorationdayController.$inject = ['explorationdayService', '$location'];

    angular.module('explorationdayModule').component('explorationday', {
        templateUrl: "/ng1/explorationday.template.html",
        controller: explorationdayController
    });
})();