(function(){
    function listController(){
        var vm = this;
    }

    angular.module('explorationdaylistModule').component('explorationdaylist', {
        template: '<ul class="heroes">'+
                        '<li ng-repeat="person in $ctrl.data">'+
                            '<span>{{person.first_name}}</span>'+
                        '</li>'+
                   '</ul>',
        controller: listController,
        bindings: {
            data: '<'
        }
    });
})();